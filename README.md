## RSEAP
A Python-based business rule/automation engine, or more generally a manager to configure chaining of python functions.

### Introduction
The package is a business rule management system, and aims to covers the same business need and functionalities as other Java-based BRMS systems such as Drools or IBM ODM.

The design philosophy is different from the Java based counterparts in that:

* RSEAP is pythonic. Python function is used as business rule, and there is no DSL (domain specific language) involved
* Users are given freedom (with all pros and cons) of programmatically specifying the rules and compute graph
* All components (compute graph, decision rule, rules etc) are programmable and dynamic, i.e. a business process project can be entirely generated programatically.
    * In fact, the package is created to enable the creation of business rule / progress that is directed from an external AI system
* Designed to be easily integrated with external systems (e.g. running an external rule through API call, call another API service etc)

Note that users should have basic Python programming experience to use the package. For users w/o prior programming knowledge, we are planning a simplied version with reduced functionalities to be access from a web-UI.


### Concept
See examples/ for quick start.

In essence, the package orchestrates a group python functions based on user-specified conditions. There are 3 main components in RSEAP rule engine: (1) RuleFlow, (2) DecisionNode, (3) DecisionRule.

RuleFlow is the main compute graph. It consists DecisionNodes and their linkage information. DecisionNode in turn consists of one or more DecisionRules. A DecisionRule consists of one Python function. 

The only requirement for the python function is to have the arguments "instance" and "state", which the function would act on. Typically, the function would read the informaiton from the "instance" and apply some changes to the "state". A DecisionNode will be invoked whenever all its direct parent nodes have been executed. All DecisionRules in the DecisionNode will be executed, in a sequence dictated by their priority values. Each decision node will execute at most once in an inference run. The inference run ends when a maximum step has been reached, or when no more DecisionNode execution is possible. At the end of the run, the (modified) instance, state and a inference tracker object will be available.


### Roadmap
* Better logger and tracking support
* Object persistence
* Support global state and function hook
* Web-based frontend UI for RuleFlow design
* Better support for searchs
* Extensive tests
* DecisionNode and DecisionRule library/marketplace with tracking capability (which project uses this rule?)
* Multitenant support (most likely another project)
* Compare RuleFlow similarity
* Utility for Drools/IBM ODM migration
* Utility to convert sklearn decision tree to RuleFlow
* "Real" rule engine functionalities (forward-/backward- chaining)
