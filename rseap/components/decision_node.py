import re
import uuid
import types
import inspect
import numbers
import collections
import logging

import rseap
from ..utils import Struct

class DecisionRule:
	def __init__(self, name=None, uid=None, func=None, prefunc=None):
		'''Create a DecisionRule object
		
		Args:
			name (String): Name of the DecisionRule
			uid (String): UID of the DecisionRule
			func (function): Function of the DecisionRule
			prefunc (function): Precondition function of the DecisionRule
		'''

		self.name = name
		if uid is None:
			uid = str(uuid.uuid4())
		self.uid = uid

		# by default always execute
		self.prefunc = lambda *args, **kwargs: True

		if func is not None:
			self.set_function(func)
		if prefunc is not None:
			self.set_precondition(prefunc)

	# end def

	def func(self, instance, state):
		raise NotImplementedError
	# end def

	def __repr__(self):
		if self.name is not None:
			return 'Rule "%s"' % (self.name,)
		else:
			return 'Rule [%s]' % (self.uid,)
		# end if
	# end def

	def set_precondition(self, func):
		'''Set the precondition function for the DecisionRule
		
		Args:
			func (function): function to determine if the rule should be invoked.
		'''
		
		assert isinstance(func, types.FunctionType), 'func must be a function'
		assert set(inspect.getfullargspec(func).args) == {'instance', 'state'}, \
		'function must have "instance" and "state" arguments'
		self.prefunc = func
	# end def

	def set_function(self, func):
		'''Set the main function for the DecisionRule
		
		Args:
			func (function): Main processing function of the DecisionRule
		'''

		assert isinstance(func, types.FunctionType), 'func must be a function'
		assert set(inspect.getfullargspec(func).args) == {'instance', 'state'}, \
		'function must have "instance" and "state" arguments'

		self.func = func
	# end def

	def execute(self, instance, state):
		'''Process a request (instance, state)
		
		Args:
			instance (Any): Object instance to be processed
			state (Any): Environment state to be processed. If not specified, will use the RuleFlow shared state.
		'''

		return self.func(instance=instance, state=state)
	# end def

# end class



class DecisionNode:
	def __init__(self, name=None, uid=None, ruleflow=None, rules=None, info=None, rule_priorities=None):
		'''Create a DecisionNode object
		
		Args:
			name (String): Name of the DecisionNode
			uid (String): UID of the DecisionNode. If not provided, an UUID string will be generated for this.
			ruleflow (RuleFlow): RuleFlow for the DecisionNode to register with.
			rules (Dict): List of DecisionRules under this DecisionNode
			info (Dict): Custom info for the DecisionNode
			rule_priorities (Dict): Priority values for the DecisionRules. By default 0 if the priority value is not provided.
		'''

		if uid is None:
			uid = str(uuid.uuid4())
		# end if
		assert isinstance(uid, str), 'uid must be a string'
		self.uid = uid

		if info is None: info = {}
		assert isinstance(info, collections.Mapping), 'info must be of Dict type'
		self.info = info

		if name is not None:
			assert isinstance(uid, str), 'node name must be a string'
		self.name = name

		# register with ruleflow (if specified)
		if ruleflow is not None:
			assert isinstance(ruleflow, rseap.RuleFlow), 'ruleflow must be an RuleFlow instance'
			self.register_ruleflow(ruleflow)
		# end if
		self.ruleflow = ruleflow

		# rules
		if rules is None: rules = {}
		assert isinstance(rules, collections.Mapping), 'rules must be of Dict type'
		assert all([isinstance(item, DecisionRule) for item in rules.values()]), 'rules must contain only DecisionRule objects'
		self.rules = rules

		if rule_priorities is None: rule_priorities = {}
		assert isinstance(rule_priorities, collections.Mapping), 'rule_priorities must be of Dict type'
		assert all([isinstance(item, numbers.Number) for item in rule_priorities.values()]), 'rule_priorities must contain only numeric values'
		self.rule_priorities = collections.defaultdict(lambda: 0)
		self.rule_priorities.update(rule_priorities)

	# end def

	def add_rule(self, rule, priority=0):
		'''Add a DecisionRule to the DecisionNode

		Args:
			rule (DecisionRule): DecisionRule to be added
			priority (Float): Priority value of the DecisionRule. The greater the value the earlier the rule will be executed in the sequence.
		'''

		assert isinstance(rule, DecisionRule), 'rule must be a DecisionRule object'
		assert rule.uid is not None, 'rule UID is not set'
		assert rule.uid not in self.rules, 'rule with same UID has already been added'
		self.rules[rule.uid] = rule
		self.rule_priorities[rule.uid] = priority
	# end def

	def list_rules(self):
		'''List all DecisionRules in the DecisionNode

		Returns:
			(List <DecisionRule>)
		'''

		output = {}
		for uid, rule in self.rules.items():
			output[uid] = {
				'uid': uid,
				'rule': rule,
				'priority': self.rule_priorities[uid],
			}
		# end for
		return output
	# end def

	def search_rules(self, pattern):
		'''Search rules by the names with regex pattern

		Args:
			pattern (String): regex pattern
			
		Returns:
			(List <DecisionRule>): List of DecisionRules matching the search pattern
		'''

		p = re.compile(pattern)
		output = []
		for rule in self.rules.values():
			if p.search(repr(rule)) is not None:
				output.append(rule)
			# end if
		# end for
		return output
	# end def

	def get_rule(self, uid):
		'''Get a DecisionRule by UID

		Args:
			uid (String): UID of the DecisionRule
			
		Returns:
			(DecisionRule)
		'''
		
		return self.rules.get(uid)
	# end def

	def get_rule_by_name(self, name):
		'''Get a DecisionRule by name. Only the first matched DecisionRule is returned in case of multiple Rules with the same name.

		Args:
			name (String): name of the DecisionRule
			
		Returns:
			(DecisionRule)
		'''

		# only return the first match
		for uid, _rule in self.rules.items():
			if _rule.name == name:
				return _rule
			# end if
		# end for
	# end def

	def set_rule_priority(self, rule_id, priority_value):
		'''Set priority value to a DecisionRule

		Args:
			rule_id (String): UID of the DecisionRule
			priority_value (Float): Priority value of the DecisionRule. The greater the value the earlier the Rule will be executed in the sequence.
		'''

		assert rule_id in self.rules, 'DecisionRule [%s] not found' % (rule_id,)
		self.rule_priorities[rule_id] = priority_value
	# end def

	def remove_rule(self, rule_id):
		'''Remove a DecisionRule from the DecisionNode

		Args:
			rule_id (String): UID of the DecisionRule to be removed
		'''
		
		assert rule_id in self.rules, 'no rule with rule_id [%s] was found' % (rule_id,)
		self.rules.pop(rule_id)
	# end def

	def _sort_rules_by_priority(self):
		'''Sort DecisionRules by priority values

		Returns:
			(List <String>): List of UIDs of the sorted DecisionRules.
		'''

		sorter = []
		for uid in self.rules.keys():
			sorter.append([-self.rule_priorities[uid], uid])
		# end for
		# no input, no output
		if len(sorter) == 0:
			return []
		# end if

		sorter.sort()
		return list(zip(*sorter))[1]
	# end def

	def __repr__(self):
		desc = self.info.get('short_desc', self.name)
		if desc is None:
			desc = self.uid
		return desc
	# end def

	def add_parent_node(self, parent_node):
		'''Add a parent node for the DecisionNode

		Args:
			parent_node (DecisionNode): Parent DecisionNode to link with
		'''
		
		assert isinstance(parent_node.uid, str), 'parent node must have a valid uid attribute'

		if (self.ruleflow is None) and (parent_node.ruleflow is None):
			raise AssertionError("either one of parent or child node be registered with a ruleflow")
		elif isinstance(self.ruleflow, rseap.RuleFlow) and isinstance(parent_node.ruleflow, rseap.RuleFlow):
			assert self.ruleflow == parent_node.ruleflow, 'parent and child node must be in the same ruleflow'
		elif isinstance(self.ruleflow, rseap.RuleFlow) and (parent_node.ruleflow is None):
			parent_node.register_ruleflow(self.ruleflow)
		elif (self.ruleflow is None) and isinstance(parent_node.ruleflow, rseap.RuleFlow):
			self.register_ruleflow(parent_node.ruleflow)
		else:
			raise Exception('cannot determine ruleflow of parent node and child node')
		# end if

		self.ruleflow.add_edge(parent_node, self)
	# end def
	__lshift__ = add_parent_node # shorthand

	def add_child_node(self, child_node):
		'''Add a child node for the DecisionNode

		Args:
			child_node (DecisionNode): Child DecisionNode to link with
		'''

		return child_node.add_parent_node(self)
	# end def
	__rshift__ = add_child_node # shorthand


	def unlink_parent_node(self, parent_node):
		'''Unlink with a parent node

		Args:
			parent_node (DecisionNode): Parent DecisionNode to unlink with
		'''

		if isinstance(parent_node, DecisionNode):
			parent_uid = parent_node.uid
		elif isinstance(parent_node, str):
			parent_uid = parent_node
		else:
			raise TypeError('parent_node must be either a DecisionNode object or a DecisionNode object uid')
		# end if

		assert self.ruleflow.graph is not None, 'this node is not registered with a ruleflow'
		assert parent_uid in self.ruleflow.graph.predecessors(self.uid), 'input node not a parent of this node'

		self.ruleflow.graph.remove_edge(parent_uid, self.uid)
	# end def

	def unlink_child_node(self, child_node):
		'''Unlink with a child node

		Args:
			child_node (DecisionNode): Child DecisionNode to unlink with
		'''

		if isinstance(child_node, DecisionNode):
			child_uid = child_node.uid
		elif isinstance(child_node, str):
			child_uid = child_node
		else:
			raise TypeError('child_node must be either a DecisionNode object or a DecisionNode object uid')
		# end if

		assert self.ruleflow.graph is not None, 'this node is not registered with a ruleflow'
		assert child_uid in self.ruleflow.graph.successors(self.uid), 'input node not a child of this node'

		self.ruleflow.graph.remove_edge(self.uid, child_uid)
	# end def

	def register_ruleflow(self, ruleflow):
		'''Register the DecisionNode with a RuleFlow. An instance of DecisionNode can only be registered with one RuleFlow.

		Args:
			ruleflow (RuleFlow): RuleFlow to register with
		'''

		self.ruleflow = ruleflow
		ruleflow.add_node(self)
	# end def

	def process(self, instance, state, tracker=None):
		'''Process an input request. Input request is defined by an instance and the environment state.

		Args:
			instance (Any): Object instance to be processed
			state (Any): Environment state to be processed
			tracker (RuleFlowTracker): A RuleFlowTracker instance to track the processing
		'''

		if tracker is not None:
			assert isinstance(tracker, rseap.RuleFlowTracker)

		# sort rule by priorities
		rule_list = self._sort_rules_by_priority()
		for rule_id in rule_list:
			rule = self.rules[rule_id]
			# check for precondition
			if rule.prefunc(instance, state) is not True:
				if tracker is not None:
					tracker.logger.debug('Rule [%s] does not satisfy precondition - skipped execution' % (rule.uid,))
				continue
			# end if
			# TODO: allow only one rule to be executed
			rule.execute(instance=instance, state=state)
		# end for
	# end def
# end class
