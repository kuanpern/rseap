import re
import math
import copy
import uuid
import types
import inspect
import numbers
import collections
import logging

import matplotlib.pyplot as plt
import networkx
from networkx.drawing.nx_agraph import graphviz_layout

import rseap
from ..utils import Struct
from .decision_node import *
from .tracker import *

class RuleFlow:

	def __init__(self, name=None, uid=None, nodes=None, graph=None, node_priorities=None, nodes_data=None, state=None, logger=None):
		'''Generate a RuleFlow

		Args:
			name (String): Name for the RuleFlow
			uid (String): Unique identifier of the RuleFlow. By default an UUID string will be generated.
			nodes (Dict): Collections of DecisionNodes in the RuleFlow compute graph
			graph (DiGraph): networkx digraph specifying the compute graph of the RuleFlow
			node_priorities (Dict): Priority values for the DecisionNodes. By default 0 if the priority value is not provided.
			nodes_data (Dict): Customized data for each of the DecisionNodes
			state (Any): State variable for the compute graph. By default an empty object will be generated.
			logger (Logger): Logger for the RuleFlow. If not specified, will use a default logger.
		'''
		
		# read input attributes
		if name is not None:
			assert isinstance(name, str), 'name must be a string'
		self.name = name

		if uid is not None:
			assert isinstance(uid, str), 'RuleFlow id must be a string'
			self.uid = uid
		else:
			self.uid = str(uuid.uuid4())
		# end if

		if nodes is None: nodes = {}
		assert all(isinstance(node, DecisionNode) for node in nodes), 'nodes must contain only DecisionNode objects'
		self.nodes = nodes

		if graph is not None:
			assert isinstance(graph, networkx.classes.digraph.DiGraph), 'graph must be a networkx directional graph instance'
		self.graph = networkx.digraph.DiGraph()

		if node_priorities is None: node_priorities = {}
		assert isinstance(node_priorities, collections.Mapping), 'node_priorities must be of Dict type'
		assert all([isinstance(item, numbers.Number) for item in node_priorities.values()]), 'node_priorities must contain only numeric values'
		self.node_priorities = collections.defaultdict(lambda: 0)
		self.node_priorities.update(node_priorities)

		if nodes_data is None: nodes_data = {}
		assert isinstance(nodes_data, collections.Mapping), 'nodes_data must be of Dict type' # TODO
		self.nodes_data = collections.defaultdict(lambda: collections.defaultdict(lambda: {}))
		self.nodes_data.update(nodes_data)
		
		# set initial state
		if state is None:
			state = Struct({})
		self.state = state
		
		if logger is None:
			logger = logging.getLogger()
		assert isinstance(logger, logging.Logger)
		self.logger = logger
		
	# end def

	def to_dict(self):
		'''Returns a dictionary representation of the RuleFlow

		Args:
			name (String): Name for the RuleFlow
			uid (String): Unique identifier of the RuleFlow. By default an UUID string will be generated.
			nodes (Dict): Collections of DecisionNodes in the RuleFlow compute graph
			graph (DiGraph): networkx digraph specifying the compute graph of the RuleFlow
			node_priorities (Dict): Priority values for the DecisionNodes. By default 0 if the priority value is not provided.
			nodes_data (Dict): Customized data for each of the DecisionNodes
			state (Any): State variable for the compute graph. By default an empty object will be generated.
			
		Returns:
			(Dict)
		'''
		
		return {
		  'name' : self.name,
		  'uid'  : self.uid,
		  'nodes': [_.uid for _ in self.nodes],
		  'graph': networkx.node_link_data(self.graph),
		  'node_priorities': self.node_priorities,
		  'nodes_data': self.nodes_data,
		  'state': self.state
		}
	# end def

	def search_nodes(self, pattern):
		'''Search nodes by the names with regex pattern

		Args:
			pattern (String): regex pattern
			
		Returns:
			(List <DecisionNode>): List of DecisionNodes matching the search pattern
		'''
		
		p = re.compile(pattern)
		output = []
		for node in self.nodes:
			if p.search(repr(node)) is not None:
				output.append(node)
			# end if
		# end for
		return output
	# end def

	def add_node(self, node, priority=0):
		'''Add a DecisionNode to the RuleFlow

		Args:
			node (DecisionNode): DecisionNode to be added
			priority (Float): Priority value of the DecisionNode. The greater the value the earlier the node will be executed in the sequence.
		'''
		
		assert isinstance(node, DecisionNode), 'node type not understood'
		assert isinstance(node.uid, str), 'node uid must be a string'
		assert node.uid not in self.nodes, 'node with the same uid is already added in the rule flow'
		self.nodes[node.uid] = node
		self.graph.add_node(node.uid)
		self.node_priorities[node.uid] = priority
	# end def
	
	def set_node_priority(self, node_id, priority_value):
		'''Set priority value to a DecisionNode

		Args:
			node_id (String): UID of the DecisionNode
			priority (Float): Priority value of the DecisionNode. The greater the value the earlier the node will be executed in the sequence.
		'''

		assert node_id in self.nodes, 'DecisionNode [%s] not found' % (node_id,)
		self.node_priorities[node_id] = priority_value
	# end def

	def add_nodes(self, nodes, priorities=None):
		'''Add a DecisionNode to the RuleFlow

		Args:
			nodes (List): List of DecisionNodes to be added
			priorities (Dict): Priority values of the DecisionNodes. The keys must be a subset of the nodes' UIDs
		'''
		
		if priorities is None:
			priorities = {}
		# end if
		
		extras = list(set(priorities.keys()) - set([_node.uid for _node in nodes]))
		if len(extras) != 0:
			raise KeyError('priorities values with no corresponding nodes: %s' % str(extras))
		# end if

		for node in nodes:
			self.add_node(node)
		# end for
		for node_id, priority_value in priorities.items():
			self.set_node_priority(node_id=node_id, priority_value=priority_value)\
		# end for
	# end def
	
	def get_node(self, uid):
		'''Get a DecisionNode by UID

		Args:
			uid (String): UID of the DecisionNode
			
		Returns:
			(DecisionNode)
		'''
		
		return self.nodes[uid]
	# end def
	
	def get_node_by_name(self, name):
		'''Get a DecisionNode by name. Only the first matched DecisionNode is returned in case of multiple nodes with the same name.

		Args:
			name (String): name of the DecisionNode
			
		Returns:
			(DecisionNode)
		'''
		
		# only return the first match
		for uid, _node in self.nodes.items():
			if _node.name == name:
				return _node
			# end if
		# end for
	# end def
	

	def add_edge(self, parent_node, child_node):
		'''Add an edge between two nodes in the compute graph. This function is mostly for internal use. Users are recommended to use "add_parent_node", "add_child_node" instead.

		Args:
			parent_node (DecisionNode): Parent node to be linked
			child_node (DecisionNode): Child node to be linked			
		'''

		assert parent_node.ruleflow == self, 'parent node not registered with the RuleFlow'
		assert  child_node.ruleflow == self,  'child node not registered with the RuleFlow'
		self.graph.add_edge(parent_node.uid, child_node.uid)
	# end def
	
	def add_edges(self, pairs):
		'''Add edges between pairs of nodes in the compute graph. This function is mostly for internal use.

		Args:
			pairs (List): List of pairs of (parent_node, child_node)
		'''
		
		for parent_node, child_node in pairs:
			self.add_edge(parent_node, child_node)
		# end for
	# end def

	def get_root_nodes(self):
		'''Get all root nodes (nodes without parent nodes) in the RuleFlow
			
		Returns:
			List (<DecisionNode>)
		'''

		root_node_ids = [] 
		for node_id in self.graph.nodes:
			if self.graph.in_degree(node_id) == 0:
				root_node_ids.append(node_id)
			# end if
		# end for
		root_nodes = [self.nodes[_id] for _id in root_node_ids]
		return root_nodes
	# end def

	def get_terminal_nodes(self):
		'''Get all terminal nodes (nodes without child nodes) in the RuleFlow
			
		Returns:
			(List <DecisionNode>)
		'''

		terminal_node_ids = []
		for node_id in self.graph.nodes:
			if self.graph.out_degree(node_id) == 0:
				terminal_node_ids.append(node_id)
			# end if
		# end for
		terminal_nodes = [self.nodes[_id] for _id in terminal_node_ids]
		return terminal_nodes
	# end def

	def _sort_nodes_by_priority(self, uids=None):
		'''Sort DecisionNodes in the RuleFlow by priorities values

		Args:
			uids (List): UIDs of the DecisionNodes to sort. If not specified all existing DecisionNodes are selected.

		Returns:
			(List <String>): List of the sorted DecisionNode UIDs
		'''

		if uids is None: uids = list(self.nodes.keys())
		uids = list(uids)
		if len(uids) == 0:
			return []
		# end if

		sorter = []
		for key in uids:
			sorter.append([-self.node_priorities[key], key])
		# end for
		sorter.sort()
		return list(zip(*sorter))[1]
	# end def

	def _build_node_list(self):
		'''Build the node list for the RuleFlow. Node list are sorted by priorities.
		'''

		# sort DecisionNodes and DecisionRules by priorities
		uids = self.graph.nodes.keys()
		self.node_list = self._sort_nodes_by_priority(uids)		
	# end def

	def _get_parents_id(self, child_uid):
		'''Get the parents' UIDs for a child node UID

		Args:
			child_uid (String): Child node UID
			
		Returns:
			(List <String>): List of parent nodes' UIDs
		'''

		parent_ids = self.graph.predecessors(child_uid)
		parent_ids = self._sort_nodes_by_priority(parent_ids)
		return parent_ids
	# end def

	def validate(self):
		'''Validate the RuleFlow. Not implemented at the moment
		'''

		return None
	# end def

	def process(self, instance, state=None, max_iter=None): # TODO: logger and hook support
		'''Process an input request. Input request is defined by an instance and the environment state.

		Args:
			instance (Any): Object instance to be processed
			state (Any): Environment state to be processed. If not specified, will use the RuleFlow shared state.
			max_iter (Integer): Maximum iteration of the processing. By default no limit is set.
			
		Returns:
			(Dict): Output dictionary specifying the status, error, output and process tracker of the run
		'''

		self._build_node_list() # sort node by priorities
		self.validate() # validate graph connectivity is sound

		# use global state if not specified
		if state is None:
			state = self.state
		# end if

		# initiate a run
		_tracker = RuleFlowTracker(self)
		_tracker.log_start_time()
		try:
			return self._process(_tracker=_tracker, instance=instance, state=state, max_iter=max_iter)
		except Exception as e:
			err = e
			return {
			  'status': 'failed', 
			  'error': err,
			  'tracker': _tracker,
			  'output': {
				'instance': instance,
				'state'   : state
			  }
			}
		# end try
	# end def

	def _process(self, _tracker, instance, state, max_iter=0):
		if max_iter in [0, None]:
			max_iter = math.inf
		# end if
		assert isinstance(max_iter, numbers.Number), 'max_iter must be numeric type'

		# assign instance to root nodes
		root_nodes = self.get_root_nodes()
		assert len(root_nodes) > 0, 'no root node was found for the rule flow'
		if len(root_nodes) > 1:
			_tracker.logger.warning("More than one root node (%d) was found in the rule flow" % (len(root_nodes),))
		# end if
		
		_iter = 0
		while True:
			for uid in self.node_list:
				# already processd, skip
				if _tracker.execution_status[uid] is True:
					continue
				# end if

				# if all parent nodes processd
				if all(map(_tracker.execution_status.get, self.graph.predecessors(uid))):
					_tracker.logger.info('DecisionNode ready for execution: "%s" [%s]' % (repr(self.nodes[uid]), uid,))
					decision_node = self.nodes[uid]
					decision_node.process(instance=instance, state=state, tracker=_tracker)
					_tracker.logger.info('DecisionNode executed: "%s" [%s]' % (repr(self.nodes[uid]), uid,))
					_tracker.execution_status[uid] = True
				# end if
			# end for

			if _tracker.execution_status == _tracker.last_execution_status:
				break
			else:
				_tracker.last_execution_status = copy.deepcopy(_tracker.execution_status)
			# end if

			_iter += 1
			if _iter > max_iter:
				self.logger.warning('Exceed max iteration [%d]. Emergency stop' % (max_iter,))
				break
			# end if
		# end while
		_tracker.log_end_time()

		output = {
		  'status': 'success', 
		  'error' : None,
		  'tracker': _tracker,
		  'output': {
			'instance': instance,
			'state'   : state
		  }
		}
		return output
	# end def
	
	def list_orphan_nodes(self):
		'''List orphan nodes in the RuleFlow. Orphan nodes are DecisionNodes without parent and child nodes
			
		Returns:
			(List <DecisionNode>): List of orphan nodes.
		'''
		
		orphan_nodes = []
		for uid in self.graph.nodes.keys():
			if max(self.graph.in_degree(uid), self.graph.out_degree(uid)) == 0:
				orphan_nodes.append(self.nodes[uid])
			# end if
		# end for
		return orphan_nodes
	# end def

	def delete_node(self, uid):	
		'''Delete a DecisionNode from the RuleFlow by UID

		Args:
			uid (String): UID of the DecisionNode to delete
		'''
		
		if uid not in self.nodes:
			self.logger.warning('node with id [%s] not found in compute graph' % (uid,))
		else:
			self.nodes.remove(uid, None)
		# end if

		if self.graph.has_node(uid) is False:
			self.logger.warning('node with id [%s] not found in compute graph' % (uid,))
		self.graph.remove_node(uid)

		self.node_priorities.pop(uid, None)
	# end def

	def delete_nodes(self, uids):
		'''Delete multiple DecisionNodes from the RuleFlow by UIDs

		Args:
			uids (List): List of UIDs of DecisionNodes to delete
		'''
		
		for uid in uids:
			self.delete_node(uid)
		# end for
	# end def

	def prune_orphaned_nodes(self):
		'''Prune orphaned DecisionNodes from the RuleFlow

		Args:
			(List <String>): List of UIDs of the deleted DecisionNodes
		'''

		orphan_uids = self.list_orphan_nodes()
		self.delete_nodes(orphan_uids)
		return orphan_uids
	# end def

	def visualize(self, options=None):
		'''Visualize the RuleFlow (connectivity among the DecisionNodes). Only works in interactive session.

		Args:
			options (Dict): networkx.draw_networkx options
		'''
	
		if options is None: options = {}
		labels = {key: repr(self.nodes[key]) for key in self.nodes.keys()}

		swap = {
		  'node_shape': 's',
		  'node_color': 'lightgreen',
		  'node_size': 250,
		  'width': 1,
		  'arrowstyle': '-|>',
		  'arrowsize': 20,
		}
		swap.update(options)
		options = swap
		pos = graphviz_layout(self.graph, prog='dot')
		plt.clf()
		#plt.gcf().set_size_inches(12, 12) 
		networkx.draw_networkx(self.graph, pos=pos, arrows=True, labels=labels, **options)
		plt.tight_layout()
	# end def
	
	def __repr__(self):
		if self.name is None:
			return 'RuleFlow [%s]' % self.uid
		else:
			return 'RuleFlow "%s"' % self.name
		# end if
	# end def
	
# end class


