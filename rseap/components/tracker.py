import time
import uuid
import logging
import rseap
from rseap.utils import TailLogger

class RuleFlowTracker:
	def __init__(self, ruleflow, logger=None):
		'''Generate a RuleFlow processing tracker

		Args:
			ruleflow (RuleFlow): RuleFlow to track.
			logger (Logger): Logger to use. If not specified a TailLogger will be used.
		'''
		
		self.uid = str(uuid.uuid4())

		self.last_execution_status = None
		self.execution_status = {_: False for _ in ruleflow.node_list}

		if logger is not None:
			self.logger = logger
		else:
			self.logger = logging.getLogger()
			self.logger.setLevel(logging.INFO)
			self.events_log = TailLogger()
			log_handler = self.events_log.log_handler
			formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
			log_handler.setFormatter(formatter)
			self.logger.addHandler(log_handler)
		# end if
	# end def

	def log_start_time(self):
		'''Set the start timestamp of the run
		'''

		self.start_time = time.time()
	# end def

	def log_end_time(self):
		'''Set the end timestamp of the run
		'''

		self.end_time = time.time()
		self.duration = self.end_time - self.start_time
	# end def
# end class



