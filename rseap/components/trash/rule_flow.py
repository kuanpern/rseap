import re
import math
import copy
import uuid
import types
import inspect
import numbers
import collections
import logging

import matplotlib.pyplot as plt
import networkx
from networkx.drawing.nx_agraph import graphviz_layout

from .utils import *


class RuleFlow:

	def __init__(self, name=None, uid=None, nodes=None, graph=None, node_priorities=None, nodes_data=None, state=None):
		# read input attributes
		if name is not None:
			assert isinstance(name, str), 'name must be a string'
		self.name = name

		if uid is not None:
			assert isinstance(uid, str), 'RuleFlow id must be a string'
			self.uid = uid
		else:
			self.uid = str(uuid.uuid4())
		# end if

		if nodes is None: nodes = {}
		assert all(isinstance(node, DecisionNode) for node in nodes), 'nodes must contain only DecisionNode objects'
		self.nodes = nodes

		if graph is not None:
			assert isinstance(graph, networkx.classes.digraph.DiGraph), 'graph must be a networkx directional graph instance'
		self.graph = networkx.digraph.DiGraph()

		if node_priorities is None: node_priorities = {}
		assert isinstance(node_priorities, collections.Mapping), 'node_priorities must be of Dict type'
		assert all([isinstance(item, numbers.Number) for item in node_priorities.values()]), 'node_priorities must contain only numeric values'
		self.node_priorities = collections.defaultdict(lambda: 0)
		self.node_priorities.update(node_priorities)

		if nodes_data is None: nodes_data = {}
		assert isinstance(nodes_data, collections.Mapping), 'nodes_data must be of Dict type' # TODO
		self.nodes_data = collections.defaultdict(lambda: collections.defaultdict(lambda: {}))
		self.nodes_data.update(nodes_data)
		
		# set initial state
		if state is None:
			state = Struct({})
		self.state = state
		
	# end def

	def to_dict(self):
		return {
		  'name' : self.name,
		  'uid'  : self.uid,
		  'nodes': [_.uid for _ in self.nodes],
		  'graph': networkx.node_link_data(self.graph),
		  'node_priorities': self.node_priorities,
		  'nodes_data': self.nodes_data,
		  'state': self.state
		}
	# end def

	@classmethod # TODO
	def from_data(cls, data):
		# instantialize graph
		data['graph'] = networkx.node_link_graph(data.pop('graph'))
		# instantialize (Rule-)Nodes
		node_uids = data.pop('nodes')

		nodes = {}
		for node_uid in node_uids:
			_node = DecisionNode(
			  rule        = data['nodes_data']['rule_uid'],
			  rule_engine = rule_engine,
			  name        = data['node_reprs'].get(node_id, node_id),
			  uid         = node_uid, 
			  ruleflow    = self, 
			  priority    = data['node_priorities'].get(node_id),  
			  custom_data = data['nodes_data']
			) # end node
			nodes[node_uid] = _node
		# end for
		data['nodes'] = nodes
		data['rule_engine'] = rule_engine

		return cls(**data)
	# end def

	def search_nodes(self, pattern):
		p = re.compile(pattern)
		output = []
		for node in self.nodes:
			if p.search(repr(node)) is not None:
				output.append(node)
			# end if
		# end for
		return output
	# end def

	def add_node(self, node, priority=0):
		assert isinstance(node, DecisionNode), 'node type not understood'
		assert isinstance(node.uid, str), 'node uid must be a string'
		assert node.uid not in self.nodes, 'node with the same uid is already added in the rule flow'
		self.nodes[node.uid] = node
		self.graph.add_node(node.uid)
		self.node_priorities[node.uid] = priority
	# end def
	
	def set_node_priority(self, node_id, priority_value):
		assert node_id in self.nodes, 'DecisionNode [%s] not found' % (node_id,)
		self.node_priorities[node_id] = priority_value
	# end def

	def add_nodes(self, nodes):
		for node in nodes:
			self.add_node(node)
		# end for
	# end def
	
	def get_node(self, uid):
		return self.nodes[uid]
	# end def
	
	def get_node_by_name(self, name):
		# only return the first match
		for uid, _node in self.nodes.items():
			if _node.name == name:
				return _node
			# end if
		# end for
	# end def
	

	def add_edge(self, parent_node, child_node):
		assert parent_node.ruleflow == self, 'parent node not registered with the RuleFlow'
		assert  child_node.ruleflow == self,  'child node not registered with the RuleFlow'
		self.graph.add_edge(parent_node.uid, child_node.uid)
	# end def

	def add_edges(self, pairs):
		for parent_node, child_node in pairs:
			self.add_edge(parent_node, child_node)
		# end for
	# end def

	def get_root_nodes(self):
		root_node_ids = [] 
		for node_id in self.graph.nodes:
			if self.graph.in_degree(node_id) == 0:
				root_node_ids.append(node_id)
			# end if
		# end for
		root_nodes = [self.nodes[_id] for _id in root_node_ids]
		return root_nodes
	# end def

	def get_terminal_nodes(self):
		terminal_node_ids = []
		for node_id in self.graph.nodes:
			if self.graph.out_degree(node_id) == 0:
				terminal_node_ids.append(node_id)
			# end if
		# end for
		terminal_nodes = [self.nodes[_id] for _id in terminal_node_ids]
		return terminal_nodes
	# end def

	def _sort_nodes_by_priority(self, uids):
		uids = list(uids)
		# no input, no output
		if len(uids) == 0:
			return []
		# end if

		sorter = []
		for key in uids:
			sorter.append([-self.node_priorities[key], key])
		# end for
		sorter.sort()
		return list(zip(*sorter))[1]
	# end def

	def _build_node_list(self):
		# sort DecisionNodes and DecisionRules by priorities
		uids = self.graph.nodes.keys()
		self.node_list = self._sort_nodes_by_priority(uids)		
	# end def

	def _get_parents_id(self, child_uid):
		parent_ids = self.graph.predecessors(child_uid)
		parent_ids = self._sort_nodes_by_priority(parent_ids)
		return parent_ids
	# end def

	def validate(self):
		return None
	# end def

	def process(self, instance, state=None, max_iter=None): # logger and hook support
		self._build_node_list() # sort node by priorities
		self.validate() # validate graph connectivity is sound

		# use global state if not specified
		if state is None:
			state = self.state
		# end if

		# initiate a run
		_tracker = RuleFlowTracker(self)
		_tracker.log_start_time()
		try:
			return self._process(_tracker=_tracker, instance=instance, state=state, max_iter=max_iter)
		except Exception as e:
			err = e
			return {
			  'status': 'failed', 
			  'error': err,
			  'tracker': _tracker,
			  'output': {
			    'instance': instance,
			    'state'   : state
			  }
			}
		# end try
	# end def

	def _process(self, _tracker, instance, state, max_iter=0):
		if max_iter in [0, None]:
			max_iter = math.inf
		# end if
		assert isinstance(max_iter, numbers.Number), 'max_iter must be numeric type'

		# assign instance to root nodes
		root_nodes = self.get_root_nodes()
		assert len(root_nodes) > 0, 'no root node was found for the rule flow'
		if len(root_nodes) > 1:
			_tracker.logger.warning("More than one root node (%d) was found in the rule flow" % (len(root_nodes),))
		# end if
		
		_iter = 0
		while True:
			for uid in self.node_list:
				# already processd, skip
				if _tracker.execution_status[uid] is True:
					continue
				# end if

				# if all parent nodes processd
				if all(map(_tracker.execution_status.get, self.graph.predecessors(uid))):
					_tracker.logger.info('DecisionNode ready for execution: "%s" [%s]' % (repr(self.nodes[uid]), uid,))
					decision_node = self.nodes[uid]
					decision_node.process(instance=instance, state=state, tracker=_tracker)
					_tracker.logger.info('DecisionNode executed: "%s" [%s]' % (repr(self.nodes[uid]), uid,))
					_tracker.execution_status[uid] = True
				# end if
			# end for

			if _tracker.execution_status == _tracker.last_execution_status:
				break
			else:
				_tracker.last_execution_status = copy.deepcopy(_tracker.execution_status)
			# end if

			_iter += 1
			if _iter > max_iter:
				logger.warning('Exceed max iteration [%d]. Emergency stop' % (max_iter,))
				break
			# end if
		# end while
		_tracker.log_end_time()

		output = {
		  'status': 'success', 
		  'error' : None,
		  'tracker': _tracker,
		  'output': {
		    'instance': instance,
			'state'   : state
		  }
		}
		return output
	# end def
	
	def list_orphan_nodes(self):
		orphan_nodes = []
		for uid in self.graph.nodes.keys():
			if max(self.graph.in_degree(uid), self.graph.out_degree(uid)) == 0:
				orphan_nodes.append(self.nodes[uid])
			# end if
		# end for
		return orphan_nodes
	# end def

	def delete_node(self, uid):
		if uid not in self.nodes:
			logger.warning('node with id [%s] not found in compute graph' % (uid,))
		else:
			self.nodes.remove(uid, None)
		# end if

		if self.graph.has_node(uid) is False:
			logger.warning('node with id [%s] not found in compute graph' % (uid,))
		self.graph.remove_node(uid)

		self.node_priorities.pop(uid, None)
	# end def

	def delete_nodes(self, uids):
		for uid in uids:
			self.delete_node(uid)
		# end for
	# end def

	def prune_orphaned_nodes(self):
		orphan_uids = self.list_orphan_nodes()
		self.delete_nodes(orphan_uids)
		return orphan_uids
	# end def

	def visualize(self, options=None):
		if options is None: options = {}
		labels = {key: repr(self.nodes[key]) for key in self.nodes.keys()}

		swap = {
		  'node_shape': 's',
		  'node_color': 'lightgreen',
		  'node_size': 250,
		  'width': 1,
		  'arrowstyle': '-|>',
		  'arrowsize': 20,
		}
		swap.update(options)
		options = swap
		pos = graphviz_layout(self.graph, prog='dot')
		plt.clf()
		#plt.gcf().set_size_inches(12, 12) 
		networkx.draw_networkx(self.graph, pos=pos, arrows=True, labels=labels, **options)
		plt.tight_layout()
	# end def
	
	def __repr__(self):
		if self.name is None:
			return 'RuleFlow [%s]' % self.uid
		else:
			return 'RuleFlow "%s"' % self.name
		# end if
	# end def
	
# end class


class RuleFlowTracker:
	def __init__(self, ruleflow, logger=None):
		self.uid = str(uuid.uuid4())

		self.last_execution_status = None
		self.execution_status = {_: False for _ in ruleflow.node_list}

		if logger is not None:
			self.logger = logger
		else:
			self.logger = logging.getLogger()
			self.logger.setLevel(logging.INFO)
			self.events_log = TailLogger()
			log_handler = self.events_log.log_handler
			formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
			log_handler.setFormatter(formatter)
			self.logger.addHandler(log_handler)
		# end if
	# end def

	def log_start_time(self):
		self.start_time = time.time()
	# end def

	def log_end_time(self):
		self.end_time = time.time()
		self.duration = self.end_time - self.start_time
	# end def
# end class


class DecisionRule:
	def __init__(self, name=None, uid=None, func=None, prefunc=None):
		self.name = name
		if uid is None:
			uid = str(uuid.uuid4())
		self.uid = uid
		
		# by default always execute
		self.prefunc = lambda *args, **kwargs: True
		
		if func is not None:
			self.set_function(func)
		if prefunc is not None:
			self.set_precondition(prefunc)
		
	# end def
	
	def func(self, instance, state):
		raise NotImplementedError
	# end def
	
	def __repr__(self):
		if self.name is not None:
			return 'Rule "%s"' % (self.name,)
		else:
			return 'Rule [%s]' % (self.uid,)
		# end if
	# end def
	
	def set_precondition(self, func):
		assert isinstance(func, types.FunctionType), 'func must be a function'
		assert set(inspect.getfullargspec(func).args) == {'instance', 'state'}, \
		'function must have "instance" and "state" arguments'
		self.prefunc = func
	# end def
	
	def set_function(self, func):
		assert isinstance(func, types.FunctionType), 'func must be a function'
		assert set(inspect.getfullargspec(func).args) == {'instance', 'state'}, \
		'function must have "instance" and "state" arguments'
		
		self.func = func
	# end def
	
	def execute(self, instance, state):
		return self.func(instance=instance, state=state)
	# end def
	
	def set_priority(self, priority_value):
		assert isinstance(priority_value, numbers.Number), 'priority_value must be of numeric type'
	# end def
# end class



class DecisionNode:
	def __init__(self, name=None, uid=None, ruleflow=None, rules=None, info=None, rule_priorities=None):
	
		if uid is None:
			uid = str(uuid.uuid4())
		# end if
		assert isinstance(uid, str), 'uid must be a string'
		self.uid = uid
		
		if info is None: info = {}
		assert isinstance(info, collections.Mapping), 'info must be of Dict type'
		self.info = info

		if name is not None:
			assert isinstance(uid, str), 'node name must be a string'
		self.name = name

		# register with ruleflow (if specified)
		if ruleflow is not None:
			assert isinstance(ruleflow, RuleFlow), 'ruleflow must be an RuleFlow instance'
			self.register_ruleflow(ruleflow)
		# end if
		self.ruleflow = ruleflow

		# rules
		if rules is None: rules = {}
		assert isinstance(rules, collections.Mapping), 'rules must be of Dict type'
		assert all([isinstance(item, DecisionRule) for item in rules.values()]), 'rules must contain only DecisionRule objects'
		self.rules = rules

		if rule_priorities is None: rule_priorities = {}
		assert isinstance(rule_priorities, collections.Mapping), 'rule_priorities must be of Dict type'
		assert all([isinstance(item, numbers.Number) for item in rule_priorities.values()]), 'rule_priorities must contain only numeric values'
		self.rule_priorities = collections.defaultdict(lambda: 0)
		self.rule_priorities.update(rule_priorities)
		
	# end def
	
	def add_rule(self, rule, priority=0):
		assert isinstance(rule, DecisionRule), 'rule must be a DecisionRule object'
		assert rule.uid is not None, 'rule UID is not set'
		assert rule.uid not in self.rules, 'rule with same UID has already been added'
		self.rules[rule.uid] = rule
		self.rule_priorities[rule.uid] = priority
	# end def
	
	def list_rules(self):
		output = {}
		for uid, rule in self.rules.items():
			output[uid] = {
				'uid': uid,
				'rule': rule,
				'priority': self.rule_priorities[uid],
			}
		# end for
		return output
	# end def
	
	def search_rules(self, pattern):
		p = re.compile(pattern)
		output = []
		for rule in self.rules.values():
			if p.search(repr(rule)) is not None:
				output.append(rule)
			# end if
		# end for
		return output
	# end def
	
	def get_rule(self, uid):
		return self.rules[uid]
	# end def
	
	def get_rule_by_name(self, name):
		# only return the first match
		for uid, _rule in self.rules.items():
			if _rule.name == name:
				return _rule
			# end if
		# end for
	# end def

	def set_rule_priority(self, rule_id, priority_value):
		assert rule_id in self.rules, 'DecisionRule [%s] not found' % (rule_id,)
		self.rule_priorities[rule_id] = priority_value
	# end def
	
	def remove_rule(self, rule_id):
		assert rule_id in self.rules, 'no rule with rule_id [%s] was found' % (rule_id,)
		self.rules.pop(rule_id)
	# end def

	def set_priority(self, priority_value):
		assert isinstance(priority_value, numbers.Number), 'priority_value must be of numeric type'
	# end def
	
	def _sort_rules_by_priority(self):
		sorter = []
		for uid in self.rules.keys():
			sorter.append([-self.rule_priorities[uid], uid])
		# end for
		# no input, no output
		if len(sorter) == 0:
			return []
		# end if
		
		sorter.sort()
		return list(zip(*sorter))[1]
	# end def
	
	def __repr__(self):
		desc = self.info.get('short_desc', self.name)
		if desc is None:
			desc = self.uid
		return desc
	# end def
	
	def add_parent_node(self, parent_node):
		assert isinstance(parent_node.uid, str), 'parent node must have a valid uid attribute'

		if (self.ruleflow is None) and (parent_node.ruleflow is None):
			raise AssertionError("either one of parent or child node be registered with a ruleflow")
		elif isinstance(self.ruleflow, RuleFlow) and isinstance(parent_node.ruleflow, RuleFlow):
			assert self.ruleflow == parent_node.ruleflow, 'parent and child node must be in the same ruleflow'
		elif isinstance(self.ruleflow, RuleFlow) and (parent_node.ruleflow is None):
			parent_node.register_ruleflow(self.ruleflow)
		elif (self.ruleflow is None) and isinstance(parent_node.ruleflow, RuleFlow):
			self.register_ruleflow(parent_node.ruleflow)
		else:
			raise Exception('cannot determine ruleflow of parent node and child node')
		# end if

		self.ruleflow.add_edge(parent_node, self)
	# end def
	__lshift__ = add_parent_node # shorthand

	def add_child_node(self, child_node):
		return child_node.add_parent_node(self)
	# end def
	__rshift__ = add_child_node # shorthand

	
	def unlink_parent_node(self, parent_node):
		if isinstance(parent_node, DecisionNode):
			parent_uid = parent_node.uid
		elif isinstance(parent_node, str):
			parent_uid = parent_node
		else:
			raise TypeError('parent_node must be either a DecisionNode object or a DecisionNode object uid')
		# end if

		assert self.ruleflow.graph is not None, 'this node is not registered with a ruleflow'
		assert parent_uid in self.ruleflow.graph.predecessors(self.uid), 'input node not a parent of this node'
		
		self.ruleflow.graph.remove_edge(parent_uid, self.uid)
	# end def

	def unlink_child_node(self, child_node):
		if isinstance(child_node, DecisionNode):
			child_uid = child_node.uid
		elif isinstance(child_node, str):
			child_uid = child_node
		else:
			raise TypeError('child_node must be either a DecisionNode object or a DecisionNode object uid')
		# end if

		assert self.ruleflow.graph is not None, 'this node is not registered with a ruleflow'
		assert child_uid in self.ruleflow.graph.successors(self.uid), 'input node not a child of this node'
		
		self.ruleflow.graph.remove_edge(self.uid, child_uid)
	# end def
	
	def register_ruleflow(self, ruleflow):
		self.ruleflow = ruleflow
		ruleflow.add_node(self)
	# end def
	
	def process(self, instance, state, tracker=None):
		if tracker is not None:
			assert isinstance(tracker, RuleFlowTracker)
			
		# sort rule by priorities
		rule_list = self._sort_rules_by_priority()
		for rule_id in rule_list:
			rule = self.rules[rule_id]
			# check for precondition
			if rule.prefunc(instance, state) is not True:
				if tracker is not None:
					tracker.logger.debug('Rule [%s] does not satisfy precondition - skipped execution' % (rule.uid,))
				continue
			# end if
			# TODO: allow only one rule to be executed
			rule.execute(instance=instance, state=state)
		# end for
	# end def
# end class


