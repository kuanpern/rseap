# NOT IN USE
import collections
import json

def is_jsonable(x):
	try:
		json.dumps(x)
		return True
	except:
		return False
	# end try
# end def

def check_range_validity(r):
	'''check if the range is valid'''

	required_fields = ['dtype', 'values']
	for field in required_fields:
		assert field in r, 'condition must contain a "%s" field' % field
	# end for

	allowed_values = ['continuous', 'discrete']
	assert r['dtype'] in allowed_values, '"dtype" can only take value of %s' % str(allowed_values)

	if 'action' in r:
		allowed_values = [True, False]
		assert r['action'] in allowed_values, '"action" can only take value of %s' % str(allowed_values)
	# end if

	if r['dtype'] == 'discrete':
		assert isinstance(r['values'], (list, tuple)), 'discrete dtype values must be list or tuple'
	elif r['dtype'] == 'continuous':
		assert isinstance(r['values'], collections.Mapping), 'continuous dtype values must be of Dict type'
	# end if
# end if


def check_condition_validity(c):
	'''check if the condition is valid'''
	required_fields = ['name', 'op', 'ranges']
	for field in required_fields:
		assert field in c, 'condition must contain a "%s" field' % field
	# end for        	
	assert isinstance(c['name'], collections.Hashable), '"input_name" must be hashable'


	allowed_values = ['AND', 'OR']
	assert c['op'] in allowed_values, '"op" can only take value of %s' % str(allowed_values)

	if 'required' in c:
		allowed_values = [True, False]
		assert c['required'] in allowed_values, '"required" can only take value of %s' % str(allowed_values)
	# end if

	assert isinstance(c['ranges'], (list, tuple)), '"ranges" must be a list of dictionaries' % item

	for r in c['ranges']:
		assert isinstance(r, collections.Mapping), '"range" item must be of Dict type'
		check_range_validity(r)
	# end for
# end def

def check_rule_data_validity(rule_data):
	'''check if rule_data is valid'''

	assert isinstance(rule_data, collections.Mapping), 'rule_data must be of Dict type'

	required_fields = ['returns', 'conditions']
	for field in required_fields:
		assert field in rule_data, 'rule_data must contain a "%s" field' % field
	# end for
	
	required_fields = ['true', 'false', 'default']
	assert set(rule_data['returns'].keys()) >= set(required_fields), '"returns" must has the following keys: %s' % str(required_fields)

	for item in ['conditions', 'pre-conditions']:
		if item not in rule_data: continue
		assert isinstance(rule_data[item], (list, tuple)), '"item" must be a list of dictionaries' % item

		for condition in rule_data[item]:
			assert isinstance(condition, collections.Mapping), '"condition" item must be of Dict type'
			check_condition_validity(condition)
		# end for
	# end for
# end def


