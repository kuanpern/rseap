import copy
import json
import hashlib
import uuid
import numpy as np
import collections
from .validators import *


class Condition:
	def __init__(self, input_name, ranges, required=True, op='OR'):
		# input type checks
		if isinstance(ranges, ValueRange): # single value_range given, cast to list
			ranges = [ranges]
		# end if
		assert isinstance(input_name, collections.Hashable), 'input_name must be hashable'
		for r in ranges:
			assert isinstance(r, ValueRange), '"range" item must be of ValueRange type'
			check_range_validity(r.to_dict())
		# end for
		assert required in [True, False], '"required" must be of Bool type'
		assert op in ['OR', 'AND'], '"op" can only take the value of "OR" or "AND"'
		
		self.input_name = input_name
		self.required   = required
		self.op         = op
		self.ranges     = ranges

		# "compile" the condition check function
		self._compile()
	# end def
	
	def to_dict(self):
		return {
		  "name"    : self.input_name,
		  "required": self.required,
		  "op"      : self.op,
		  "ranges"  : [_.to_dict() for _ in self.ranges]
		}
	# end def
	
	@classmethod
	def from_data(cls, data):
		check_condition_validity(data)
		return cls(
			input_name = data['name'],
			ranges     = list(map(ValueRange.from_data, data['ranges'])),
			required   = data.get('required', True),
			op         = data['op']
		) # end cls
	# end def

	def _compile(self):
		self._func = gen_condition_checker(self.to_dict())
	# end def

	def apply(self, data, dynamic_compile=None):
		return self._func(data)
	# end def

	__call__ = apply
	
# end class


class Return:
	def __init__(self, uid=None, true_returns=None, false_returns=None, default_returns=None, inplace=False):
		if uid is None:
			uid = str(uuid.uuid4())
		assert isinstance(uid, str), 'uid must be a string'
		assert type(inplace) is bool, '"inplace" must be of bool type'
		self.inplace = inplace

		self.uid = uid
		self.options = {
		  True     : true_returns,
		  False    : false_returns,
		  'default': default_returns
		}

	# end def
	
	def apply(self, key, data):
		assert key in [True, False, 'default']
		out = self.options[key]
		if out is None:
			return out
		if self.inplace is True: # TODO: to enrich this
			swap = copy.deepcopy(data)
			swap.update(out)
			out = swap
		# end if
		return out
	# end def

	def to_dict(self):
		return {
		  "uid"     : self.uid,
		  "inplace" : self.inplace,
		  "true"    : self.options.get(True),
		  "false"   : self.options.get(False),
		  "default" : self.options.get('default')
		}
	# end def
	
	@classmethod
	def from_data(cls, data):
		def get_value(D, keys):
			'''get a single value (except None) from dictionary D for key in keys. priority of key sorted ascending.'''
			out = None
			for key in keys:
				out = D.get(key, out)
			# end for
			return out
		# end def

		return cls(
		  uid             = data.get('uid'),
		  inplace         = data.get('inplace', False),
		  true_returns    = get_value(data, [1,  True,  'true']),
		  false_returns   = get_value(data, [0, False, 'false']),
		  default_returns = get_value(data, ['default']),
		) # end cls
	# end def
# end class

class ValueRange:
	def __init__(self, values, action=True, dtype=None):

		assert action in [True, False], 'action must be of Bool type'
		assert dtype in [None, 'continuous', 'discrete'], 'dtype can only takes the value of "continuous" or "discrete"'

		if dtype is not None:
			self.dtype = dtype
		else: # infer type
			self.dtype = 'discrete'
			if isinstance(values, dict):
				if ('lb' in values) or ('ub' in values):
					self.dtype = 'continuous'
				# end if
			# end if
		# end if

		self.values = values
		self.action = action
	# end def

	def __repr__(self):
		return json.dumps(self.to_dict())
	# end def

	@classmethod
	def from_data(cls, data):
		return cls(
		  values = data['values'],
		  action = data['action'],
		  dtype  = data['dtype']
		) # end cls
	# end def

	def to_dict(self):
		out = {}
		out['values'] = self.values
		out['action'] = self.action
		out['dtype']  = self.dtype
		return out
	# end def

# end def


def gen_conditions_checker(conditions): # support multiple conditions
	if conditions is None: # if no conditions given, then always True
		return lambda data: True
	# end if
	if len(conditions) == 0:
		return lambda data: True

	def func(data):
		funcs = [gen_condition_checker(condition.to_dict()) for condition in conditions]
		return all([func(data) for func in funcs])
	# end def
	return func
# end def


# note: assume format checked already
def gen_condition_checker(condition):
	def func(data):
		value = data.get(condition['name'])
		if value is None:
			return np.logical_not(condition.get('required', True)) # if not specified, assume required
		dets = []
		for item in condition['ranges']:
			if item['dtype'] == 'continuous':
				lb = item['values'].get('lb', -np.inf)
				ub = item['values'].get('ub',  np.inf)

				det = np.logical_not(np.logical_xor(
					item.get('action', True),
					lb <= value <= ub
				))
			elif item['dtype'] == 'discrete':
				det = value in item['values']
			else:
				raise Exception('dtype not understood')
			# end if
			dets.append(det)
		# end for
		if condition['op'] == 'AND':
			return all(dets)
		elif condition['op'] == 'OR':
			return any(dets)
		else:
			raise Exception('logical operation not understood')
		# end if
	# end def
	return func
# end def

