import time
import uuid
import numpy as np
import sys
import collections
import pymongo
import json
import logging
logger = logging.getLogger()

from .components import *
from .validators import *
from .utils import FixedSizeDict

class RuleEngine:
	def __init__(self, rule_coll, backup_coll, cache_size=None, logger=None):
		assert isinstance(rule_coll, pymongo.collection.Collection), 'rule_coll must be a pymongo collection'
		self.rule_coll   = rule_coll
		self.backup_coll = backup_coll

		# create backup collection (mongo < ver 4.4)
		if backup_coll.name not in backup_coll.database.list_collection_names():
			backup_coll.database.create_collection(backup_coll.name)
		# end if

		# setup logger
		if logger is None:
			logger = logging.getLogger()
		# end if
		self.set_logger(logger)

		if cache_size is None: # no limit
			self.rule_cache = {}
		else:
			self.rule_cache = FixedSizeDict(size=cache_size)
		# end if
	# end def

	def set_cache_size(self, cache_size):
		if cache_size is None:
			swap = {}
		else:
			swap = FixedSizeDict(size=cache_size)
		# end if
		swap.update(self.rule_cache)
		self.rule_cache = swap
	# end if

	def set_logger(self, logger):
		assert isinstance(logger, logging.Logger), 'logger must be a Logger instance'
		self.logger = logger
	# end def

	# CRUD of rules
	def add_rule(self, rule):
		assert isinstance(rule, Rule), 'input must be of Rule type'
		if rule.uid is None:
			rule.uid = str(uuid.uuid4())
		# end if
		if self.rule_coll.find_one({'uid': rule.uid}) is not None:
			err_msg = 'Rule with same the id already exists. Use update_rule instead'
			logger.error(err_msg)
			raise KeyError(err_msg)
		# end if
		content = rule.to_dict()
		content['_signature'] = rule._get_signature()
		r = self.rule_coll.insert_one(content)
		return r
	# end def

	def add_rules(self, rules):
		assert all([isinstance(rule, Rule) for rule in rules]), 'all inputs must be of Rule type'
		succeeds, faileds = [[], []]
		# note: not using insert_many now for clarity purpose
		for i in range(len(rules)):
			rule = rules[i]
			try:
				r = self.add_rule(rule)
				succeeds.append(i)
			except Exception as e:
				faileds.append((i, e))
			# end try
		# end for
		return {'succeeds': succeeds, 'faileds': faileds}
	# end def

	def get_rules(self, rule_ids, use_cache=True, cache_rule=True):
		output = []
		for rule_id in rule_ids: # will fail if any can't be loaded
			rule = self.get_rule(rule_id, use_cache=use_cache, cache_rule=cache_rule)
			output.append(rule)				
		# end for
		return output
	# end def


	def get_rule(self, rule_id, use_cache=True, cache_rule=True):
		if (use_cache is True) and (rule_id in self.rule_cache):
			return self.rule_cache[rule_id]
		# end if

		r = self.rule_coll.find_one({'uid': rule_id})
		if r is None:
			raise IndexError('Rule with id "%s" not found.' % rule_id)
		# end if
		output = Rule.from_data(r)

		# update to cache
		if cache_rule is True:
			self.rule_cache[rule_id] = output
		# end if
		return output
	# end def


	def update_rule(self, rule, upsert=True):
		assert isinstance(rule, Rule), 'input rule must be of Rule type'
		assert type(upsert) == bool, '"upsert" must be of bool type'
		assert rule.uid is not None, 'input rule does not have an "uid" attribute'

		# actually replace
		r = self.rule_coll.find_one({'uid': rule.uid})
		if r is None:
			if upsert is False:
				raise KeyError('rule with id "%s" not found' % rule.uid)
			# end if
		else:
			self.delete_rule(rule_id=rule.uid, backup=True)
			try:
				return self.add_rule(rule)
			except:
				# revert delete operation
				self.rule_coll.insert_one(r)
				raise
			# end try
		# end if
	# end def

	def delete_rule(self, rule_id, backup=True):
		assert type(backup) is bool, '"backup" option must of of bool type'

		# clear from local cache
		self.rule_cache.pop(rule_id, None)

		r = self.rule_coll.find_one({'uid': rule_id})
		r['_backup'] = {'timestamp': int(time.time())}
		if r is None:
			raise IndexError('Rule with id "%s" not found.' % rule_id)
		# end if

		if backup is False:
			return coll.delete_one({'_id': rule_id})
		# end if

		# atomic delete and backup
		client = self.rule_coll.database.client
		db     = self.rule_coll.database.name

		def callback(session):
			rule_coll   = session.client[db][self.  rule_coll.name]
			backup_coll = session.client[db][self.backup_coll.name]

			# Important:: You must pass the session to the operations.
			rule_coll  .delete_one({'uid': rule_id}, session=session)
			backup_coll.insert_one(r               , session=session)
		# end def
		with client.start_session() as session:
			session.with_transaction(
			  callback,
			  read_preference = pymongo.ReadPreference.PRIMARY,
			  read_concern    = pymongo.read_concern.ReadConcern('local'),
			  write_concern   = pymongo.WriteConcern("majority", wtimeout=1000),
			)
		# end with
	# end def

	def delete_rules(self, rule_ids, backup=True):
		succeeds, faileds = [[], []]
		for rule_id in rule_ids:
			try:
				self.delete_rule(rule_id, backup=backup)
				succeeds.append(rule_id)
			except Exception as e:
				faileds.append((rule_id, e))
			# end try
		# end for
		return {'succeeds': succeeds, 'faileds': faileds}
	# end def

	def search_rules(self, filter, projection={}, sort=None, skip=None, limit=None):
		assert isinstance(projection, collections.Mapping), 'projection must be a dictionary'
		swap = {'_id': False}
		swap.update(projection)
		swap.update({'uid': True})
		projection = swap

		filter.update({'uid': {'$exists': True}})
		cursor = self.rule_coll.find(filter=filter, projection=projection)
		cursor = self._decorate_cursor(cursor, sort, skip, limit)

		rule_ids = []
		return list(cursor)
	# end def

	def cache_rules(self, rule_ids, force_reload=False):
		if force_reload is True:
			self._force_reload(set(rule_ids) & set(self.rule_cache.keys()))
		# end if

		for rule_id in rule_ids:
			# lazy loading
			if rule_id in self.rule_cache:
				continue 
			rule_data = self.rule_coll.find_one({'uid': rule_id})
			if rule_data is None:
				self.logger.warning('Rule with ID [%s] not found in database, skipped' % (rule_id,))
				continue
			# end if
			self.rule_cache[rule_id] = Rule.from_data(rule_data)
		# end for
	# end def

	def _force_reload(self, rule_ids=None):
		if rule_ids is None: # reload all
			rule_ids = self.rule_cache.keys()
		# end if
		for rule_id in rule_ids:
			rule_data = self.rule_coll.find_one({'uid': rule_id})
			if rule_data is None:
				self.logger.warning('Rule with ID [%s] not found in database, skipped' % (rule_id,))
				continue
			# end if
			self.rule_cache[rule_id] = Rule.from_data(rule_data)
		# end if
	# end def


	def _decorate_cursor(self, cursor, sort, skip, limit):
		if sort is not None:
			cursor = cursor.sort(sort)
		if skip is not None:
			cursor = cursor.skip(skip)
		if limit is not None:
			cursor = cursor.skip(limit)
		return cursor
	# end def


	def search_rules_and_apply(self, filter, instance, sort=None, skip=None, limit=None, cache_rule=True):
		'''Apply in-place without loading all to memory'''

		filter.update({'uid': {'$exists': True}})
		projection = {'_id': False}
		cursor = self.rule_coll.find(filter=filter, projection=projection)
		cursor = self._decorate_cursor(cursor, sort, skip, limit)

		outputs = []
		for item in cursor:
			rule = Rule.from_data(item)
			if cache_rule is True:
				self.rule_cache[rule.uid] = rule
			# end if
			output = rule.apply(instance)
			outputs.append({'uid': rule.uid, 'return': output})
		# end for
		return outputs
	# end def


	def apply(self, rules_ids, instance, use_cache=True, cache_rule=True):
		outs, errs = [[], []]
		for rule_id in rule_ids:
			try:
				rule = self.get_rule(rule_id, use_cache=use_cache, cache_rule=cache_rule)
				out = rule.apply(instance)
				outs.append({'uid': rule_id, 'return': out})
			except Exception as e:
				errs.append({'uid': rule_id, 'error': e})
			# end try
		# end for
		return outs, errs
	# end def
# end class
