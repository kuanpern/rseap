import time
import copy
import logging
import numbers
import collections
from collections import OrderedDict

# taken and modified from https://stackoverflow.com/questions/1305532/convert-nested-python-dict-to-object
class Struct:
	def __init__(self, data=None):
		'''Generic object from dictionary

		Args:
		    data (Dict): (Nested) dictionary with key/value pair to assign as object attributes
		'''

		if data is None: data = {}
		assert isinstance(data, collections.Mapping), 'data must be of Dict type'
		for name, value in data.items():
			setattr(self, name, self._wrap(value))
		# end for
	# end def

	def _wrap(self, value):
		if isinstance(value, (tuple, list, set, frozenset)):
			return type(value)([self._wrap(v) for v in value])
		else:
			return Struct(value) if isinstance(value, dict) else value
		# end if
	# end def

	def to_dict(self):
		'''Generate dictionary representation of the object

		Args:
		    data (Dict): (Nested) dictionary with key/value pair to assign as object attributes
		    
        Returns:
            (Dict)
		'''

		out = copy.deepcopy(self.__dict__)
		for key, val in out.items():
			if isinstance(val, Struct):
				out[key] = val.to_dict()
			# end if
		# end for
		return out
	# end def
# end class


# taken from https://stackoverflow.com/questions/2437617/how-to-limit-the-size-of-a-dictionary
class FixedSizeDict(OrderedDict):
	def __init__(self, *args, **kwds):
		self.size_limit = kwds.pop("size", None)
		OrderedDict.__init__(self, *args, **kwds)
		self._check_size_limit()
	# end def

	def __setitem__(self, key, value):
		OrderedDict.__setitem__(self, key, value)
		self._check_size_limit()
	# end def

	def _check_size_limit(self):
		if self.size_limit is not None:
			while len(self) > self.size_limit:
				self.popitem(last=False)
			# end while
		# end if
	# end def
# end class



# taken from https://stackoverflow.com/questions/37944111/python-rolling-log-to-a-variable
class TailLogger:
	class TailLogHandler(logging.Handler):
		def __init__(self, log_queue):
			logging.Handler.__init__(self)
			self.log_queue = log_queue
		# end def
		
		def emit(self, record):
			self.log_queue.append(self.format(record))
		# end def
	# end class

	def __init__(self, maxlen=None):
		'''Variable Logger

		Args:
		    maxlen (Integer): Maximum number of lines for the variable. Use None for unlimited number of lines.
		    
        Returns:
            (Dict)
		'''
		
		if maxlen is not None:
			assert isinstance(maxlen, numbers.Number), 'maxlen must be a number'

		self._log_queue = collections.deque(maxlen=maxlen)
		self._log_handler = self.TailLogHandler(self._log_queue)
	# end def

	def contents(self):
		'''Return the contents of the log
		    
        Returns:
            (String)
		'''
		return '\n'.join(self._log_queue)
	# end def

	@property
	def log_handler(self):
		return self._log_handler
# end class
