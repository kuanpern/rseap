import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()
# end with

setuptools.setup(
    name="rseap",
    version="0.1.0",
    author="Tan Kuan Pern",
    author_email="kptan86@gmail.com",
    description="Search for JSON-encoded rules in MongoDB and apply them",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/kuanpern/rseap/src/master/",
    packages=setuptools.find_packages(),
    install_requires=[
      'numpy>=1.18',
      'pymongo>=3.10',
      'networkx>=2.4'
    ]
)
