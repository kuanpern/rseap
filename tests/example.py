import seap
import pymongo
import logging

conn_str  = 'mongodb://username:password@host:port/database'
rule_coll = pymongo.MongoClient(conn_str).get_default_database()['collection_name']


logger = logging.getLogger()
logger.addHandler(logging.StreamHandler(stream=sys.stdout))
engine = seap.RuleEngine(rule_coll, cache_size=100, logger=logger)


_filter = {'_meta.key.TOSP': case['Episode']['code']}
results = engine.search_rules_and_apply(_filter, instance)





